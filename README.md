# SampaMap

Projeto em desenvolvimento para a disciplina MAC0413.

## Integrantes

* Gabriel Capella (gabriel.capella@usp.br)
* Gustavo Silva (gustavo.faustino.silva@usp.br)
* Leonardo Padilha (leonardo.aguilar@usp.br)
* Lucas Moreira Santos (lucas.moreira.santos@usp.br)
* Luís Felipe de Melo (luis.melo.silva@usp.br) - líder

## Construção e Execução

Se você somente quiser adicionar o sistema ao dashboard ou à sua aplicação não é
necessário seguir esses passos. Vá para a sessão de integração e utilize a versão
disponibilizada *online*.

Na última versão foram adicionados módulos para gerenciamento de bibliotecas
externas. Para executar a nossa aplicação é necessário ter dose.js instalado, 
seu respectivo gerenciador de pacotes e executar os seguintes comandos:

```
npm install
npm install -g bower grunt
bower install
grunt
```

Os arquivos resultantes disponíveis na pasta 'dist'. Para ver um exemplo, acesse
``dist/index.html``. 

## Testes

Os testes foram feitos com o framework *Mocha*. Para executá-los, basta executar
o comando ``npm test`` após instalar as dependências do projeto.

## Integração

Exemplos práticos podem ser vistos (aqui)[http://capella.pro/mapa/example.html].

#### Mapa para auxílio de formuário

Existem grupos cuja necessidade é clicar no mapa e receber de volta a latitude e
longitude. Para isso, basta adicionar o seguinte código na área do mapa.

```
<script src="http://capella.pro/mapa/min.js" ></script>
<script>
	var map = SampaMap('map', function (latlon) {
		console.log(latlon);
		$("#lat").text(latlon.lat);
		$("#lon").text(latlon.lon);
	});
</script>
```

Depois desse código é necessário adicionar um *callback* que será chamado quando o
usuário clicar no mapa.

#### Mapa para Dashboard

Para incluir o mapa na dashboard basta adicionar um *iframe* da seguinte maneira:

```
<iframe width="600" height="450" frameborder="0" style="border:0" src="http://capella.pro/mapa/">
</iframe>
```

# Refatoração

Na última release foi realizada uma refatoração. Algumas classes foram alteradas
e outras criadas. Para resumo segue descrição breve de cada uma:

- SampaMap: normalmente recebe um o id de uma div e cria o mapa nela.
Dependendo dos argumentos que recebe, pode criar um dos 3 tipos seguintes de mapa:
    - FormMap: mapa que serve para ser integrado com formuário. 
    - InteractiveMap: mapa com menu lateral para o usuário interagir.
    - RouteMap: mapa que mostra uma rota da posição do usuário até um ponto.
- interface/Menu: gerencia tudo relativo ao menu.
- interface/HeatMap: mostra um mapa de calor em cima do mapa. Acoplado com Map.
Depende do open layers 3.
- interface/Map: cria o mapa (parte gráfica). Depende do open layers 3.
- api/Api: abstrai a comunicação com API da plataforma.
- api/Capability: capacidade da plataforma.
- api/Historical: singleton que carrega todo o histórico de dados da plataforma e pode ser consultado. Somente atualizado quando a página carrega.
- api/Resource: recurso da plataforma. Todos os dados da plataforma são copiados no campo ``data`` de cada objeto.

Para facilitar a manipulação do DOM do HTML, estamos utilizando a biblioteca
JQuery.

## WeatherCrawler

Devido a falta de dados na plataforma, fomos obrigados a criar um crawler para inserir dados. Ele foi feito de maneira rápida em Ruby.