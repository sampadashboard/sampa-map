module.exports = function(grunt) {

   var result = require('wiredep')({
      src: ['app/*.html']
   });

   // Project configuration.
   grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
      wiredep: {
         task: {

            // Point to the files that should be updated when
            // you run `grunt wiredep`
            src: [
               'app/*.html',
            ]
         }
      },
      jsbeautifier: {
         files: [
            "app/**/*.js",
            "Gruntfile.js",
            "app/**/*.html"
         ],
         options: {
            js: {
               indentSize: 3
            },
            html: {
               indentSize: 4
            }
         }
      },

      processhtml: {

         files: {
            src: ['app/index.html'],
            dest: 'dist/index.html'
         }
      },

      concat: {
         dist: {
            src: [result.js, "app/js/**/*.js"],
            dest: 'dist/min.js',
         },
      },
      cssmin: {
         dist: {
            src: [result.css, "app/**/*.css"],
            dest: 'dist/min.css',
         },
      },

      copy: {
         main: {
            files: [{
                  src: ['app/example.html'],
                  dest: 'dist/example.html'
               },
               {
                  expand: true,
                  flatten: true,
                  src: ['app/images/*'],
                  dest: 'dist/images/'
               }
            ]
         }
      },
      replace: {
         dist: {
            options: {
               patterns: [{
                  match: /jQuery/g,
                  replacement: 'cQuery'
               }, {
                  match: /jquery/g,
                  replacement: 'cquery'
               }, {
                  match: /\$\(\"/g,
                  replacement: 'cQuery\(\"'
               }, {
                  match: /\$\.ajax/g,
                  replacement: 'cQuery\.ajax'
               }, {
                  match: /\$\.get/g,
                  replacement: 'cQuery\.get'
               }, {
                  match: /\$\(\'/g,
                  replacement: 'cQuery\(\''
               }, {
                  match: /\$\(this\)/g,
                  replacement: 'cQuery\(this\)'
               }]
            },
            files: [{
               src: ['dist/min.js'],
               dest: 'dist/min.js'
            }]
         }
      }
   });

   // Load the plugins
   grunt.loadNpmTasks('grunt-wiredep');
   grunt.loadNpmTasks("grunt-jsbeautifier");
   grunt.loadNpmTasks('grunt-processhtml');
   grunt.loadNpmTasks('grunt-contrib-cssmin');
   grunt.loadNpmTasks('grunt-contrib-concat');
   grunt.loadNpmTasks('grunt-contrib-copy');
   grunt.loadNpmTasks('grunt-replace');


   // Default task(s).
   grunt.registerTask('default', ['wiredep', 'jsbeautifier', 'concat', 'cssmin', 'processhtml', 'copy', 'replace']);
};
