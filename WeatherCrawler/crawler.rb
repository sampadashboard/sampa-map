require 'mechanize'
require 'net/http'
require 'json'

url = "https://www.cgesp.org/v3/"

sensors = [
["estacao.jsp?POSTO=1000887", "Penha", -23.5252366, -46.5475598, "2606aa5e-ee31-4b46-a7b4-a6d716179644"],
["estacao.jsp?POSTO=504", "Perus", -23.4026482, -46.7481368, "94e6a72b-e14e-46e4-8622-7fa590dd6224"],
["estacao.jsp?POSTO=515", "Pirituba", -23.4791158, -46.7418047, "07ddc959-e37b-43b7-bbf4-e791132107cb"],
["estacao.jsp?POSTO=509", "Freguesia do Ó", -23.5014967, -46.69770159999999, "9550b8f5-a16c-4b1d-bc0a-92ed2827607b"],
["estacao.jsp?POSTO=510", "Santana/Tucuruvi", -23.4672867, -46.6100128, "b85d56d1-e3b9-407d-8bc1-4ca3c27178ee"],
["estacao.jsp?POSTO=1000944", "Tremembé", -22.9576323, -45.5479616, "fbbdc545-ddbd-4a7d-836e-95540cb53773"],
["estacao.jsp?POSTO=1000862", "São Miguel Paulista", -23.493309, -46.4453133, "a6635472-777a-4aed-80e3-d846136dbf71"],
["estacao.jsp?POSTO=1000882", "Itaim Paulista", -23.4691053, -46.4036655, "2a384373-b47e-4571-a341-a5bd33732f34"],
["estacao.jsp?POSTO=1000844", "São Mateus", -23.6047547, -46.4650474, "7214100b-b27e-4528-91cb-6fcd02b8d701"],
["estacao.jsp?POSTO=503", "Sé - CGE", -23.2130753, -46.84126699999999, "a186bacd-2a71-41bb-a499-3da7fa0f684c"],
["estacao.jsp?POSTO=1000840", "Ipiranga", -23.5854976, -46.5990106, "1dcd1127-4085-4b77-9505-2075a423ac73"],
["estacao.jsp?POSTO=1000852", "Santo Amaro", -23.6536633, -46.7066927, "fc112bb8-05af-487e-954d-6170ec709045"],
["estacao.jsp?POSTO=1000850", "M Boi Mirim", -23.6983898, -46.7793831, "5d371757-79e7-4bd7-a6c4-d901c7caaf9e"],
["estacao.jsp?POSTO=592", "Cidade Ademar", -23.675, -46.655833, "4108b6ae-c429-44c6-baee-fe564f614f94"],
["estacao.jsp?POSTO=507", "Barragem Parelheiros", -23.8664745, -46.6739459, "3f8c8b32-9d49-4be6-bf8d-cfd182fe3726"],
["estacao.jsp?POSTO=1000848", "Lapa", -23.5227414, -46.71044, "99c92924-d3ff-4af1-991f-60441c4b3d2a"],
["estacao.jsp?POSTO=1000854", "Campo Limpo", -23.6347959, -46.75499019999999, "e7e1b270-adf4-44ff-807b-c1dcc18d42a0"],
["estacao.jsp?POSTO=1000857", "Capela do Socorro", -23.587778, -46.716667, "cb989164-b487-4b15-96db-b7dffca73c3c"],
["estacao.jsp?POSTO=1000859", "Vila Formosa", -23.5668787, -46.5439884, "fddc46a6-3e6b-46db-bc9b-6e6a4b10178c"],
["estacao.jsp?POSTO=1000860", "Móoca", -23.5603265, -46.5995903, "f00badad-945d-47b7-855b-8f4024ad2c3f"],
["estacao.jsp?POSTO=1000864", "Itaquera", -23.5398463, -46.4475328, "de589865-0e19-4990-a66e-dab805024046"],
["estacao.jsp?POSTO=524", "Vila Prudente", -23.5790136, -46.5797561, "d0aca6f2-41b6-40e0-9e75-737ca39d2f5d"],
["estacao.jsp?POSTO=1000866", "Anhembi", -22.7883059, -48.131193, "106defa8-9ca1-42a2-b9fd-e83eebdce023"],
["estacao.jsp?POSTO=540", "Vila Maria / Guilherme", -23.514444, -46.578611, "a6d88bb3-4f8f-4939-a73b-3366e4fca328"],
["estacao.jsp?POSTO=495", "Vila Mariana", -23.5870563, -46.63574370000001, "77302f64-a5c0-4740-b905-328264cd5fad"],
["estacao.jsp?POSTO=400", "Riacho Grande", -23.7814659, -46.5280333, "2aed3d31-976a-41b8-862d-d2a020fa3038"],
["estacao.jsp?POSTO=1000876", "Mauá - Paço Municipal", -23.6674621, -46.4648441, "3fb35a17-fe5d-4c3d-857e-3072c307e47a"],
["estacao.jsp?POSTO=1000880", "Santana do Parnaíba", -23.4429961, -46.92265760000001, "d7d65354-9dc0-4f81-9dca-ae990fecd909"],
["estacao.jsp?POSTO=634", "Jabaquara", -23.6364964, -46.645191, "a14ae1a0-4814-4a1c-94c2-11d5dfa5e97e"],
["estacao.jsp?POSTO=635", "Pinheiros", -23.5630037, -46.68643470000001, "e3bd81e4-280e-4f92-aabe-d7305733746b"]
]

tmp = {}

tmp["Chuva (Por Período*)"] = 1
tmp["Temperatura"] = 1
tmp["Umidade"] = 1
tmp["Vento"] = 3
tmp["Pressão"] = 1

cap = {}

cap["Chuva (Por Período*)"] = "precipitation"
cap["Temperatura"] = "temperature"
cap["Umidade"] = "humidity"
cap["Vento"] = "wind"
cap["Pressão"] = "atmospheric_pressure"

sensors.each do |x|
	mechanize = Mechanize.new
	page = mechanize.get(url+x[0])
	titles = page.css('#instrumentos th').map do |it|
		it.text
	end

	count = 0
	page.css('.dados_estacoes').each do |table|
		data = table.css('td')[tmp[titles[count]]].text.strip.to_f

	    uri = URI('http://35.198.3.112/adaptor/resources/'+x[4]+'/data/'+cap[titles[count]])
	    http = Net::HTTP.new(uri.host, uri.port)
	    req = Net::HTTP::Post.new(uri.path, 'Content-Type' => 'application/json')
	    req.body = {data: [{value: data.to_s, timestamp: Time.now.getutc.to_s}]}.to_json
	    res = http.request(req)
	    puts "response #{res.body}"
	    count += 1
	    # puts "curl -H \"Content-Type: application/json\" -X POST -d '"+req.body+"' " + uri.to_s
	end
end
