/*******************************************************************/
/*                                                                 */
/* Arquivo:   formap.js                                            */
/* Descrição: Recebe, além da div, um callback que será chamado    */
/*            quando o mapa for inicializado.                      */
/*                                                                 */
/*******************************************************************/
class FormMap {
   constructor(id, callback) {
      var map = new MapImg(id);
      map.click(function(lat, lon) {
         map.addMarker(lat, lon);
         map.clear();
         callback(lat, lon);
      });
   }
}
