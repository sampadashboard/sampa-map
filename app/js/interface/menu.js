/*******************************************************************/
/*                                                                 */
/* Arquivo:   menu.js                                              */
/* Descrição: Responsável por criar o menu de opções.              */
/*                                                                 */
/*******************************************************************/

class Menu {
   constructor(id) {
      var element = $('#' + id);
      element.css('position', 'relative');
      var menu = $("<div/>").addClass('menu');
      this.itens = $("<div/>").addClass('menu-content');
      menu.append('<div class="logo"><img src="images/logo.png" /></div>');
      menu.append(this.itens);
      element.append(menu);
   }

   addItem(text, id, heat_btn) {
      var item = $("<div/>").addClass('menu-item');
      var title = $("<div/>").addClass('item-title');
      var box = $("<div/>").addClass('menu-check');
      var check = $("<span/>").addClass('fa fa-check');
      item.attr('val', id);
      var that = this;

      box.click(function() {
         var checkitem = $(this).find("menu-check");
         var value = false;
         if (box.html() == '') {
            box.append(check);
            value = true;
         } else {
            box.html('');
         }
         that.itemClick(id, value);
      });
      //var check = $("<span/>").addClass('fa fa-check');

      title.text(text);
      item.append(box);
      item.append(title);
      this.itens.append(item);

      if (heat_btn != true) return;
      var heat = $("<div/>").addClass('menu-heat');
      var chevron_down = $("<span/>").addClass('fa fa-chevron-down');
      var chevron_up = $("<span/>").addClass('fa fa-chevron-up');
      heat.append(chevron_down);
      heat.append(chevron_up);
      chevron_up.hide();
      item.append(heat);

      var itembar = item.clone();
      var bar = $("<div/>").addClass('slice-bar');
      itembar.html(bar);
      item.after(itembar);
      itembar.hide();

      var status = true; // armazena o status do botao
      heat.click(function() {
         if (status) {
            chevron_up.show();
            chevron_down.hide();

            bar.slider({
               value: 50,
               change: function(event, ui) {
                  if (that.heatcall)
                     that.heatcall(id, ui.value);
               }
            });

            if (that.heatstartcall)
               that.heatstartcall(id);
            itembar.show();
         } else {
            chevron_up.hide();
            chevron_down.show();
            itembar.hide();
            if (that.heatstopcall)
               that.heatstopcall(id);
         }
         status = !status;

      });
   }

   itemClick(id, value) {
      if (this.clickcall)
         this.clickcall(id, value);
   }

   click(callback) {
      this.clickcall = callback;
   }

   startHeat(callback) {
      this.heatstartcall = callback;
   }

   stopHeat(callback) {
      this.heatstopcall = callback;
   }

   changeHeat(callback) {
      this.heatcall = callback;
   }
}
