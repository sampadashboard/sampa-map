/*******************************************************************/
/*                                                                 */
/* Arquivo:   heatMap.js                                           */
/* Descrição: Responsável por processar as criar um mapa de calor. */
/*                                                                 */
/*******************************************************************/


class HeatMap {
   /* A data precisa conter o campo feature */
   constructor(map, data) {
      this.map = map.map;
      var vector = new ol.source.Vector();

      if (data) {
         var min = Infinity;
         data.forEach(function(d) {
            min = Math.min(min, d.value);
         });

         var max = -Infinity;
         data.forEach(function(d) {
            max = Math.max(max, d.value);
         });

         data.forEach(function(d) {
            /* Created for owl range of data */
            var coord = MapImg.fromLatLong(d.lon, d.lat);

            var lonLat = new ol.geom.Point(coord);

            var pointFeature = new ol.Feature({
               geometry: lonLat,
               weight: (d.value - min) / (max - min) // e.g. temperature
            });
            vector.addFeature(pointFeature);
         });
      }

      this.heatMapLayer = new ol.layer.Heatmap({
         source: vector,
         opacity: 0.5,
         radius: 50,
         blur: 100
      });

      this.map.addLayer(this.heatMapLayer);
   }

   changeOpacity(value) {
      this.heatMapLayer.setOpacity(value / 100);
   }

   remove() {
      this.map.removeLayer(this.heatMapLayer);
   }
}
