/*******************************************************************/
/*                                                                 */
/* Arquivo:   map.js                                               */
/* Descrição: Responsável por criar um mapa da cidade de São Paulo.*/
/*                                                                 */
/*******************************************************************/

class MapImg {
   constructor(id) {
      var sao_paulo_center = MapImg.fromLatLong(-46.641059, -23.5653115);

      var ini_zoom = 13;
      this.pins = new ol.source.Vector({});
      this.pinLayer = new ol.layer.Vector({
         source: this.pins
      });

      this.routes = new ol.source.Vector({});
      this.routeLayer = new ol.layer.Vector({
         source: this.routes
      });
      this.routeStyle = new ol.style.Style({
         stroke: new ol.style.Stroke({
            width: 6,
            color: [40, 40, 230, 0.8]
         })
      })

      this.map = new ol.Map({
         layers: [
            new ol.layer.Tile({
               source: new ol.source.OSM()
            })
         ],
         target: id,
         view: new ol.View({
            center: sao_paulo_center,
            zoom: 13
         })
      });
      this.map.addLayer(this.pinLayer);
      this.map.addLayer(this.routeLayer);

      /* Cria o elemento da popup */
      var ele = $('#' + id);
      ele.css('position', 'relative');
      var container = $("<div/>").addClass('ol-popup');
      var pop_content = $("<div/>").addClass('popup-content');
      container.append(pop_content);
      ele.append(container);


      var map = this.map;
      /* Popup mostrando a posição que o usuário clicou */
      var popup = new ol.Overlay({
         element: container[0]
      });
      map.addOverlay(popup);

      map.on('click', function(evt) {

         var element = popup.getElement();

         /* Attempt to find a feature in one of the visible vector layers */
         var count = 0;
         map.forEachFeatureAtPixel(evt.pixel, function(feature, layer) {
            var coord = feature.getGeometry().getCoordinates();
            var props = feature.getProperties();
            var coordinate = evt.coordinate;
            var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(
               coordinate, 'EPSG:3857', 'EPSG:4326'));
            if (feature.get('loadcontent')) {
               feature.get('loadcontent')();
               popup.setPosition(coordinate);
            }
            count += 1;
         });
         if (count == 0) {
            container.hide();
         }
      });
      this.pop_content = pop_content;
      this.pop = container;

   }

   showPop(html) {
      this.pop_content.html(html);
      this.pop.show();
   }

   static fromLatLong(lat, lon) {
      return ol.proj.fromLonLat([lat, lon]);
   }

   click(callback) {
      this.map.on('click', (evt) => {
         var coordinates = evt.coordinate;
         coordinates = ol.proj.toLonLat(coordinates);
         callback(coordinates[0], coordinates[1]);
         this.addMarker(coordinates[0], coordinates[1]);
      });
   }
   addMarker(lat, lon, id, loadcontent) {
      if (id) {
         var search = this.pins.getFeatureById(id);
         if (search) {
            search.set('count', search.get('count') + 1);
            search.set('loadcontent', loadcontent);
            return;
         }
      }
      var coordinate = MapImg.fromLatLong(lat, lon);
      var iconFeature = new ol.Feature({
         geometry: new ol.geom.Point(coordinate),
         id: id,
         count: 1, // contador se multiplas capaticidades terem o mesmo recurso. 
         loadcontent: loadcontent
      });
      iconFeature.setId(id);
      var iconStyle = new ol.style.Style({
         // Para deixar com marker do google maps
         // image: new ol.style.Icon(({
         //    anchor: [0.5, 1],
         //    anchorXUnits: 'fraction',
         //    anchorYUnits: 'pixels',
         //    opacity: 0.75,
         //    size: [48, 48],
         //    src: './images/marker.png'
         // }))
         image: new ol.style.Circle({
            radius: 8,
            fill: new ol.style.Fill({
               color: 'rgba(255,0,0,0.5)'
            }),
            stroke: new ol.style.Stroke({
               color: 'rgba(255,0,0,0.9)',
               width: 3
            })
         })
      });
      iconFeature.setStyle(iconStyle);
      this.pins.addFeature(iconFeature);
   }

   clear() {
      this.pop.hide();
      this.pins.clear();
   }

   // remove os pontos com que apresentam tal id
   remove(id) {
      this.pop.hide();
      this.pins.forEachFeature((f) => {
         if (f.get('id') == id) {
            f.set('count', f.get('count') - 1);
            if (f.get('count') == 0)
               this.pins.removeFeature(f);
         }
      });
   }

   getView() {
      return this.map.getView();
   }

   addPolyline(polyline) {
      // route is ol.geom.LineString
      var route = new ol.format.Polyline({
         factor: 1e5
      }).readGeometry(polyline, {
         dataProjection: 'EPSG:4326',
         featureProjection: 'EPSG:3857'
      });
      var feature = new ol.Feature({
         type: 'route',
         geometry: route
      });
      feature.setStyle(this.routeStyle);
      this.routes.addFeature(feature);
   }
}
