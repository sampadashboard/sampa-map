/*******************************************************************/
/*                                                                 */
/* Arquivo:   routemap.js                                          */
/* Descrição: Recebe o objeto {lat: X, lon: Y} a traça a rota do   */
/*            usuário até o ponto especificado.                    */
/*                                                                 */
/*******************************************************************/

class RouteMap {
   constructor(id, lat, lon) {
      console.log("Route");
      this.map = new MapImg(id);
      this.destination = [lat, lon];
      this.route_url = 'https://router.project-osrm.org/route/v1/driving/';

      if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition((data) => {
            this.traceRoute(data.coords);
         });
      }
   }

   traceRoute(coords) {
      var view = this.map.getView();
      view.setCenter(MapImg.fromLatLong(coords.longitude, coords.latitude));
      var path = this.route_url;
      path += [coords.longitude, coords.latitude].join();
      path += ";";
      path += this.destination.reverse().join();

      $.get(path, (data) => {
         var geometry = data.routes[0].geometry;
         this.map.addPolyline(geometry);
      })
   }

}
