/*******************************************************************/
/*                                                                 */
/* Arquivo:   interactivemap.js                                    */
/* Descrição: Gera um mapa que interage com o usuário.             */
/*                                                                 */
/*******************************************************************/

class InteractiveMap {
   constructor(id) {
      var map = new MapImg(id);
      var menu = new Menu(id);
      var api = new Api();
      var capabilities = {};

      this.list = {
         "dew_point": new WeatherData("Ponto de Orvalho", "dew_point", "Fahrenheit"),
         "ceilling": new WeatherData("Altura das Nuvens", "ceilling", "ft"),
         "uv_index": new WeatherData("Índice UV", "uv_index", ""),
         "visibility": new WeatherData("Visibilidade", "visibility", "mi"),
         "pressure": new WeatherData("Pressão", "pressure", "pol"),
         "wind_speed": new WeatherData("Velocidade do Vento", "wind_speed", "mph"),
         "sensation": new WeatherData("Sensação Térmica", "thermal_sensation", "Fahrenheit"),
         "cloud_cover": new WeatherData("Cobertura Nuvens", "cloud_cover", "%"),
         "humidity": new WeatherData("Umidade", "humidity", "%"),
         "temperature": new WeatherData("Temperatura", "temperature", "Fahrenheit"),
         "quality": new CetesbData("Qualidade do Ar"),
         "bikes": new BikeSampaData("Bicicletas")
      }

      var hist = new Historical(function() {
         $(".menu").fadeIn(300);
      });

      for (var key in this.list) {
         // if (heat_capabilities.indexOf(c.name) != -1)
         //    menu.addItem(c, c.name, true);
         // else
         menu.addItem(this.list[key], key, true);
         // capabilities[c.name] = c;
      }

      menu.click((key, value) => {
         this.click(key, value);
      });

      // qunado tiver que iniciar o heatmap
      menu.startHeat((key) => {
         this.plotHeatMap(key);
      });

      menu.stopHeat((key) => {
         // vamos montar um objeto no formato lat, lon, value
         if (this.list[key].heat)
            this.list[key].heat.remove();
      });

      // qunado o heatmap tiver uma alteraçao
      menu.changeHeat((key, value) => {
         if (this.list[key].heat)
            this.list[key].heat.changeOpacity(value);
      });

      this.map = map;
   }

   // quando um menu é selecionado
   // nome do campo e se foi selecionado ou nao
   click(key, value) {
      if (value) {
         this.list[key].getResources((resourses) => {
            this.plotResourses(resourses);
         });
      } else {
         this.list[key].forEach((resource) => {
            this.map.remove(resource.data.uuid);
         });
      }
   }

   plotHeatMap(key) {
      // vamos montar um objeto no formato lat, lon, value
      this.list[key].getResources(() => {
         var plotdata = [];
         this.list[key].forEach((resource) => {
            if (typeof resource.lastv != 'undefined') {
               plotdata.push({
                  lat: resource.data.lat,
                  lon: resource.data.lon,
                  value: resource.lastv
               })
            }
         });
         this.list[key].heat = new HeatMap(this.map, plotdata);
      });
   }

   // recebe uma lista de recursos e os plota
   plotResourses(resourses) {
      resourses.forEach((resource) => {
         resource.plot(this.map);
      });
   }
}
