/*******************************************************************/
/*                                                                 */
/* Arquivo:   SampaMap.js                                          */
/* Descrição: Verifica os parâmetros do seu construtor e           */
/*            inicializa as classes dependendo desses parâmetros.  */
/*            É possível iniciar 3 tipos distintos de mapa:        */
/*            Visualizer, Route ou Form. Se o mapa for incluido    */
/*            via iframe, também é possível inicializá-lo no       */
/*            formato Route passando os dados pela url.            */
/*                                                                 */
/*******************************************************************/
jQuery.noConflict();

class SampaMap {
   constructor(div_id, options) {
      jQuery(document).ready(() => {
         this.id = div_id;
         /* verifica se tem lat e lon na url */
         var lat = this.getURLParameter('lat');
         var lon = this.getURLParameter('lon');
         if (options) {
            lat = lat || options.lat
            lon = lon || options.lon
         }
         if (!!lat && !!lon) {
            this.type = new RouteMap(this.id, lat, lon);
            return;
         }
         if (typeof options == 'function') {
            this.type = new FormMap(this.id, options);
            return;
         }
         this.type = new InteractiveMap(this.id, lat, lon);
      });
   }

   /* pega os parâmetros de uma url */
   getURLParameter(sParam) {
      var sPageURL = window.location.search.substring(1);
      var sURLVariables = sPageURL.split('&');
      for (var i = 0; i < sURLVariables.length; i++) {
         var sParameterName = sURLVariables[i].split('=');
         if (sParameterName[0] == sParam) {
            return sParameterName[1];
         }
      }
      return undefined;
   }
}
