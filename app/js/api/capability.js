/*******************************************************************/
/*                                                                 */
/* Arquivo:   capability.js                                        */
/* Descrição: Responsável por processar as capacidades da API do   */
/*            InterSCity.                                          */
/*                                                                 */
/*******************************************************************/

class Capability {
   constructor(name, description) {
      this.name = name;
      this.description = description;
      this.uuids = [];
   }

   name() {
      return this.name;
   }

   description() {
      return this.description;
   }

   uuids() {
      return this.uuids;
   }

   toString() {
      if (this.str) return str;

      var str = this.name.replace("-", " ");
      str = str.replace("_", " ");
      str = str.replace("_", " ");
      str = str.charAt(0).toUpperCase() + str.slice(1);
      this.str = str;
      return str;
   }

   plotResources(map) {
      var self = this;
      new Api().getCapabilityResource(this.name, function(resources) {
         resources.forEach(function(c) {
            var uuid = c.data.uuid;
            if (self.uuids.indexOf(uuid) == -1)
               self.uuids.push(uuid);
            c.plot(map);
         });
      });
   }
   resources(callback) {
      new Api().getCapabilityResource(this.name, function(resources) {
         callback(resources);
      });
   }
}

if (typeof exports !== 'undefined') {
   module.exports = Capability;
}
