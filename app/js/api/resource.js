/*******************************************************************/
/*                                                                 */
/* Arquivo:   resource.js                                          */
/* Descrição: Responsável por processar os recursos da API do      */
/*            InterSCity.                                          */
/*                                                                 */
/*******************************************************************/

class Resource {
   constructor(data) {
      this.data = data;
   }

   data() {
      return this.data;
   }

   plot(map) {
      /* Preciso que seja instanciado esse método quando a classe é criada */
      var data = this.data;
      var that = this;

      var getInfo = function() {
         /* Pega mais informações desse recurso */
         new Api().getResource(data.uuid, (data) => {
            var html = data.description;
            if (that.html) {
               html += that.html;
            }
            map.showPop(html);
         });
      }
      map.addMarker(data.lon, data.lat, data.uuid, getInfo);
   }
}

if (typeof exports !== 'undefined') {
   module.exports = Resource;
}
