/*****************************************************************/
/*                                                               */
/* Arquivo:   historical.js                                      */
/* Descrição: Responsável por pegar o histórico de algum dado    */
/*            da API do InterSCity.                              */
/*                                                               */
/*****************************************************************/

class Historical {
   constructor(callback) {
      /* Singleton */
      if (!Historical.instance) {
         this.downloadData(callback);
         Historical.instance = this;
      }
      return Historical.instance;
   }

   downloadData(callback) {
      new Api().getHistorical((data) => {
         this.data = data.resources;
         callback();
      });
   }

   uuid(uuid) {
      return this.data.find(function(resource) {
         return resource.uuid == uuid;
      });
   }
}

if (typeof exports !== 'undefined') {
   module.exports = Historical;
}
