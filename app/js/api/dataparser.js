/*******************************************************************/
/*                                                                 */
/* Arquivo:   dataparser.js                                        */
/* Descrição: conjunto de classes para traduzir dados nao          */
/* estruturados em dados compreensiveis o nosso sistema            */
/*                                                                 */
/*******************************************************************/

class Data {
   constructor(name, capability) {
      this.name = name;
      this.capability = capability;
   }

   toString() {
      return this.name;
   }

   capability() {
      return this.capability;
   }

   // ja monta o html de cada recurso.
   getResources(callback) {
      new Api().getCapabilityResource(this.capability, (resources) => {
         for (var i = resources.length - 1; i >= 0; i--) {

            /* Pega os dados historicos desse recurso */
            var values = new Historical().uuid(resources[i].data.uuid);

            if (values && 'capabilities' in values) {
               var historical = values.capabilities[this.capability];
               if (historical.length > 0) {
                  var last = historical[historical.length - 1];
                  resources[i].html = this.html(last);
                  resources[i].lastv = this.value(last);
               } else {
                  resources[i].html = "<br><br>Sem dados.";
               }
            } else {
               resources[i].html = "<br><br>Sem dados.";
            }
         }

         this.resources = resources;
         if (resources) {
            callback(resources);
         }
      });
   }

   // itera sobre os recursos
   forEach(callback) {
      if (typeof this.resources == 'undefined') {
         this.getResources(() => {
            this.forEach(callback);
         });
      } else {
         this.resources.forEach(callback);
      }
   }

   html(last) {
      return "";
   }

   value(last) {
      return 0;
   }
}


class WeatherData extends Data {
   constructor(name, id, unity) {
      super(name, "weather");
      this.id = id;
      this.unity = unity;
   }

   html(last) {
      var html = "<br><small>";
      html += last['neighborhood'];
      html += "</small>";
      html += "<br><small>";
      html += this.name + ": ";
      html += last[this.id];
      html += " " + this.unity;
      html += "</small>";

      html += "<br><small>Data: ";
      html += last['date'];
      html += "</small>";
      return html;
   }

   value(last) {
      return parseFloat(last[this.id]);
   }
}

class CetesbData extends Data {
   constructor(name) {
      super(name, "air_quality");
   }

   html(last) {
      var html = "<br><small>Índice de poluição: ";
      html += last['polluting_index'];
      html += "</small>";

      html += "<br><small>Poluidor: ";
      html += last['polluting'];
      html += "</small>";

      html += "<br><small>Qualidade: ";
      html += last['air_quality'];
      html += "</small>";

      html += "<br><small>Data: ";
      html += last['date'];
      html += "</small>";
      return html;
   }

   value(last) {
      return parseFloat(last['polluting_index']);
   }
}

class BikeSampaData extends Data {
   constructor(name) {
      super(name, "slots_monitoring");
   }

   html(last) {
      var html = "<br><small>Bikes Livres: ";
      html += last['free_bikes'];
      html += "</small>";

      html += "<br><small>Número de Slots: ";
      html += last['bike_slots'];
      html += "</small>";

      html += "<br><small>Data: ";
      html += last['date'];
      html += "</small>";
      return html;
   }

   value(last) {
      return parseFloat(last['free_bikes']);
   }
}

if (typeof exports !== 'undefined') {
   module.exports = Data;
}