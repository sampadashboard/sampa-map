/*****************************************************************/
/*                                                               */
/* Arquivo:   api.js                                             */
/* Descrição: Responsável por tratar as requisições com a API do */
/*            InterSCity                                         */
/*                                                               */
/*****************************************************************/

class Api {
   constructor(url) {
      this.url = url || "https://capella.pro/mastertech/proxy.php?http://35.198.3.112";
   }
   /* Acha todas as capacidades do sistema */
   allCapabilities(callback) {
      var url = "/catalog/capabilities";
      url += "?capability_type=sensor";
      this.get(url, function(data) {
         data = data.capabilities;
         var capabilities = [];
         data.forEach(function(c) {
            capabilities.push(new Capability(c.name, c.description));
         });
         callback(capabilities);
      }).fail(function() {
         callback([]);
      });
   }

   /* Retorna os recursos com uma capacidade */
   getCapabilityResource(capability_name, callback) {
      var url = "/catalog/resources/search";
      url += "?capability=" + capability_name;
      this.get(url, function(data) {
         data = data.resources;
         var resources = [];
         data.forEach(function(c) {
            resources.push(new Resource(c));
         });
         callback(resources);
      }).fail(function() {
         callback([]);
      });
   }

   /* Retorna informacoes de um recurso */
   getResource(uuid, callback) {
      var url = "/catalog/resources/";
      url += uuid;
      this.get(url, function(data) {
         callback(data.data);
      });

   }

   getHistorical(callback) {
      var url = this.url;
      url += '/collector/resources/data';
      $.ajax({
         method: 'POST',
         dataType: "json",
         url: url,
         data: [],
         crossDomain: true,
         crossOrigin: true,
         xhrFields: {
            withCredentials: false
         },
         success: callback,
         error: function() {
         }
      })
   }

   get(path, success) {
      var url = this.url;
      url += path;
      return $.ajax({
         dataType: "json",
         url: url,
         data: [],
         crossDomain: true,
         crossOrigin: true,
         xhrFields: {
            withCredentials: false
         },
         success: success
      });
   }
}

if (typeof exports !== 'undefined') {
   module.exports = Api;
}
