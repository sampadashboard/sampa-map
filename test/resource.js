/*******************************************************************/
/*                                                                 */
/* Arquivo:   test/resource.js                                     */
/* Descrição: Testa o arquivo ../app/js/api/resource.js            */
/*                                                                 */
/*******************************************************************/

var assert = require('assert');
var Resource = require('../app/js/api/resource');

describe('Resource', function() {
    describe('#constructor(data)', function() {
        it('should return the data', function() {
            var data = 'sala de concerto';
            const r = new Resource(data);
            assert.equal(data, r.data);
        });   
    });
});
