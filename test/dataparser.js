/*******************************************************************/
/*                                                                 */
/* Arquivo:   test/dataparser.js                                   */
/* Descrição: Testa o arquivo ../app/js/api/dataparser.js          */
/*                                                                 */
/*******************************************************************/

var assert = require('assert');
var Dataparser = require('../app/js/api/dataparser');

describe('Dataparser', function() {
    describe('#constructor(name, capability)', function() {
        it('should return the name', function() {
            var name = 'sorvete';
            var capability = '2 litros';
            const d = new Dataparser(name, capability);
            assert.equal(name, d.name);
        });
        it('should return the capability', function() {
            var name = 'sorvete';
            var capability = '2 litros';
            const d = new Dataparser(name, capability);
            assert.equal(capability, d.capability);
        });
    });
});
