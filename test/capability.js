/*******************************************************************/
/*                                                                 */
/* Arquivo:   test/capability.js                                   */
/* Descrição: Testa o arquivo ../app/js/api/capability.js          */
/*                                                                 */
/*******************************************************************/

var assert = require('assert');
var Capability = require('../app/js/api/capability');

describe('Capability', function() {
    describe('#constructor(name, description)', function() {
        it('should return the name', function() {
            var name = 'egito';
            var description = 'pais da africa';
            const c = new Capability(name, description);
            assert.equal(name, c.name);
        });   
        it('should return the description', function() {
            var name = 'egito';
            var description = 'pais da africa';
            const c = new Capability(name, description);
            assert.equal(description, c.description);
        });
        it('should return uuids', function() {
            var name = 'egito';
            var description = 'pais da africa';
            const c = new Capability(name, description);
            assert.deepEqual([], c.uuids);
        });
    });
    describe('#toString()', function() {
        it('should return a formatted string', function() {
            var name = 'egito';
            var description = 'pais da africa';
            const c = new Capability(name, description);
            assert.equal('Egito', c.toString());
        });
    });
});
