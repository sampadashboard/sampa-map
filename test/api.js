/*******************************************************************/
/*                                                                 */
/* Arquivo:   test/api.js                                          */
/* Descrição: Testa o arquivo ../app/js/api/api.js                 */
/*                                                                 */
/*******************************************************************/
var assert = require('assert');
var Api = require('../app/js/api/api');

describe('Api', function() {
    describe('#constructor(url)', function() {
        it('should return our default url', function() {
            var url = "https://capella.pro/mastertech/proxy.php?http://35.198.3.112";
            const a = new Api();
            assert.equal(a.url, url);
        });  
        it('should return an url', function() {
            var url = "www.google.com";
            const a = new Api(url);
            assert.equal(a.url, url);
        })
    });
});